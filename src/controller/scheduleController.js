const connect = require("../db/connect");

module.exports = class scheduleController {
  static async createSchedule(req, res) {
    const {
      dateStart,
      dateEnd,
      days,
      user,
      classroom,
      timeStart,
      timeEnd,
    } = req.body;

    console.log("Dados recebidos:", req.body); // Adiciona um log para verificar os dados recebidos

    // Verifica se todos os campos estão preenchidos
    if (
      !dateStart ||
      !dateEnd   ||
      !days      ||
      !user      ||
      !classroom ||
      !timeStart ||
      !timeEnd
    ) {
      return res
        .status(400)
        .json({ error: "Todos os campos devem ser preenchidos" });
    }

    const daysString = days.map((day) => `${day}`).join(",");

    // Se todas as validações passarem, continue com a lógica de criação do usuário
    try {
      const overlapQuery = `SELECT * FROM schedule
        WHERE classroom = '${classroom}'
        AND  (
            (dateStart<='${dateEnd}' AND dateEnd >='${dateStart}')
        )
        AND  (
            (timeStart<='${timeEnd}' AND timeEnd >='${timeStart}')
        )
        AND  (
            (days LIKE '%Seg%' AND '${daysString}' LIKE '%Seg%')OR
            (days LIKE '%Ter%' AND '${daysString}' LIKE '%Ter%')OR
            (days LIKE '%Qua%' AND '${daysString}' LIKE '%Qua%')OR
            (days LIKE '%Qui%' AND '${daysString}' LIKE '%Qui%')OR
            (days LIKE '%Sex%' AND '${daysString}' LIKE '%Sex%')OR
            (days LIKE '%Sab%' AND '${daysString}' LIKE '%Sab%')
        )
        `;
      connect.query(overlapQuery, function (err, results) {
        if (err) {
          console.error("Erro ao verificar agendamento existente:", err); // Adiciona um log para verificar erros ao consultar a sobreposição de horários
          return res
            .status(500)
            .json({ error: "Erro ao verificar agendamento existente" });
        }
        console.log("Resultados da sobreposição:", results); // Adiciona um log para verificar os resultados da consulta

        if (results.length > 0) {
          console.log("Sala ocupada para os mesmos dias e horários"); // Adiciona um log se a sala estiver ocupada
          return res
            .status(400)
            .json({ error: "Sala ocupada para os mesmos dias e horários" });
        }

        const insertQuery = `
          INSERT INTO schedule ( dateStart,
            dateEnd,
            days,
            user,
            classroom,
            timeStart,
            timeEnd)
            VALUES(
                '${dateStart}',
                '${dateEnd}',
                '${days}',
                '${user}',
                '${classroom}',
                '${timeStart}',
                '${timeEnd}'
            )
          `;

        connect.query(insertQuery, function (err) {
          if (err) {
            console.error("Erro ao cadastrar agendamento:", err); // Adiciona um log para verificar erros ao inserir o agendamento
            return res
              .status(500)
              .json({ error: "Erro ao cadastrar agendamento" });
          }
          console.log("Agendamento cadastrado com sucesso"); // Adiciona um log se o agendamento for cadastrado com sucesso
          return res
            .status(201)
            .json({ message: "Agendamento cadastrado com sucesso" });
        });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error); // Adiciona um log para verificar erros na lógica de criação do agendamento
      return res.status(500).json({ error: "Erro interno de servidor" });
    }
  }
};
